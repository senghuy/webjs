tailwind.config = {
    theme: {
      extend: {
        fontFamily: {
          font: ["Aclonica", "sans-serif"],
          font1: ["Fraunces","serif"]
        },
        backgroundImage: {
          'bg1': "url('https://usagif.com/cdn-cgi/mirage/bee3c3fab7ff58a2764d98e0a2ebd5314f3a1036cb346968cbc4423b6cfeef58/1280/https://usagif.com/wp-content/uploads/gif/outerspace-6.gif')",
        },
        },
    },
  };